<?php
class Language extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model("LanguageModel","","TRUE");
	}
	public function index(){
		$data['language'] = $this->LanguageModel->getLanguage();
		$this->load->view("language", $data);
	}
}
?>